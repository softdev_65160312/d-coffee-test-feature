/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.ReceiptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class ReceiptDetailDao implements Dao<ReceiptDetail> {

    @Override
    public ReceiptDetail get(int id) {
        ReceiptDetail receiptDetail = new ReceiptDetail();
        String sql = "SELECT * FROM RECEIPT_DETAIL WHERE receiptDetail_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receiptDetail = receiptDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receiptDetail;
    }

    @Override
    public List<ReceiptDetail> getAll() {
        ArrayList<ReceiptDetail> list = new ArrayList<ReceiptDetail>();
        String sql = "SELECT * FROM RECEIPT_DETAIL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail receiptDetail = new ReceiptDetail();
                list.add(receiptDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ReceiptDetail insert(ReceiptDetail obj) {
        String sql = "INSERT INTO RECEIPT_DETAIL (product_id, promotionDetail_id, receipt_id, receiptDetail_unit, receiptDetail_unitPrice, receiptDetail_discount, receiptDetail_netPrice)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct().getId());
            stmt.setInt(2, obj.getPromotionDetail().getId());
            stmt.setInt(3, obj.getReceipt().getId());
            stmt.setInt(4, obj.getUnit());
            stmt.setDouble(5, obj.getUnitprice());
            stmt.setDouble(6, obj.getDiscount());
            stmt.setDouble(7,obj.getNetPrice());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ReceiptDetail update(ReceiptDetail obj) {
        String sql = "UPDATE RECEIPT_DETAIL"
                + " SET product_id = ?, promotion_id = ?, receipt_id = ?, receiptDetail_unit = ?, receiptDetail_unitPrice = ?, receiptDetail_discount = ?, receiptDetail_netPrice = ?"
                + " WHERE receiptDetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
             stmt.setInt(1, obj.getProduct().getId());
            stmt.setInt(2, obj.getPromotionDetail().getId());
            stmt.setInt(3, obj.getReceipt().getId());
            stmt.setInt(4, obj.getUnit());
            stmt.setDouble(5, obj.getUnitprice());
            stmt.setDouble(6, obj.getDiscount());
            stmt.setDouble(7,obj.getNetPrice());
            stmt.setInt(8,obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ReceiptDetail obj) {
        String sql = "DELETE FROM RECEIPT_DETAIL WHERE receiptDetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;  
    }

    @Override
    public List<ReceiptDetail> getAll(String where, String order) {
        ReceiptDetail receiptDetail;
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT_DETAIL  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                receiptDetail = ReceiptDetail.fromRS(rs);
                list.add(receiptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}