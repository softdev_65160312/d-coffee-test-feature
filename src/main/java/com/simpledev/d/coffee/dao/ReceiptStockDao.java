package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.ReceiptStock;

import com.simpledev.d.coffee.models.ReceiptStock;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ReceiptStockDao implements Dao<ReceiptStock> {

    @Override
    public ReceiptStock get(int id) {
        ReceiptStock receiptStock = null;

        String sql = "SELECT * FROM RECEIPT_STOCK WHERE receiptStock_id=?";

        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receiptStock = ReceiptStock.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receiptStock;

    }

    @Override
    public List<ReceiptStock> getAll() {

        ArrayList<ReceiptStock> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT_STOCK";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptStock receiptStock = ReceiptStock.fromRS(rs);
                list.add(receiptStock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    @Override
    public ReceiptStock insert(ReceiptStock obj) {

        String sql = "INSERT INTO RECEIPT_STOCK "
                + "(employee_id, "
                + "vendor_id, receiptStock_quantity, "
                + "receiptStock_total, receiptStock_discount, receiptStock_netPrice)"
                + "receiptStock_dateOrder, receiptStock_dateReceive, receiptStock_datePaid)"
                + "receiptStock_change, receiptStock_status)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2, obj.getVendor().getCode());
            stmt.setInt(3, obj.getQuantity());
            stmt.setDouble(4, obj.getTotal());
            stmt.setDouble(5, obj.getDiscount());
            stmt.setDouble(6, obj.getNetPrice());
            stmt.setString(7, obj.getDateOrder());
            stmt.setString(8, obj.getDateReceive());
            stmt.setString(9, obj.getDatePaid());
            stmt.setDouble(10, obj.getChange());
            stmt.setString(11, obj.getStatus());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;

    }

    @Override
    public ReceiptStock update(ReceiptStock obj) {

        String sql = "UPDATE RECEIPT_STOCK"
                + " SET receiptStock_login = ?,  receiptStock_gender = ?, receiptStock_password = ?"
                + " WHERE receiptStock_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2, obj.getVendor().getCode());
            stmt.setInt(3, obj.getQuantity());
            stmt.setDouble(4, obj.getTotal());
            stmt.setDouble(5, obj.getDiscount());
            stmt.setDouble(6, obj.getNetPrice());
            stmt.setString(7, obj.getDateOrder());
            stmt.setString(8, obj.getDateReceive());
            stmt.setString(9, obj.getDatePaid());
            stmt.setDouble(10, obj.getChange());
            stmt.setString(11, obj.getStatus());
            stmt.setInt(11, obj.getId());
            
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public int delete(ReceiptStock obj) {

        String sql = "DELETE FROM RECEIPT_STOCK WHERE receiptStock_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;

    }

    @Override
    public List<ReceiptStock> getAll(String where, String order) {
        ArrayList<ReceiptStock> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT_STOCK WHERE " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptStock receiptStock = ReceiptStock.fromRS(rs);
                list.add(receiptStock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}

