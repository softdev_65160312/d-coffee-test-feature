/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.CheckStock;
import java.util.List;
import com.simpledev.d.coffee.models.Receipt;
import com.simpledev.d.coffee.models.ReceiptDetail;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author sirikon
 */
public class ReceiptDao implements Dao<Receipt> {

    @Override
    public Receipt get(int id) {
        Receipt receipt = null;
        String sql = "SELECT * FROM RECEIPT WHERE reciept_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while(rs.next()){
                receipt = Receipt.fromRS(rs);
                ReceiptDetailDao rdd = new ReceiptDetailDao();
                ArrayList<ReceiptDetail> receiptDetails;
                receiptDetails = (ArrayList<ReceiptDetail>)rdd.getAll("receipt_id="+receipt.getId(),"receipt_detail_id");
                receipt.setRecieptDetails(receiptDetails);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receipt;

    }

    @Override
    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt pd = new Receipt();
                list.add(pd);
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

//    @Override
    public Receipt insert(Receipt obj) {
        String sql = "INSERT INTO RECEIPT (customer_id, employee_id, branch_code, receipt_date, receipt_totalPrice, receipt_discount, receipt_netPrice, receipt_moneyReceive, receipt_moneyChange)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCustomer().getId());
            stmt.setInt(2, obj.getEmployee().getId());
            stmt.setString(3, obj.getBranch().getId());
            stmt.setDate(4, (Date) obj.getDate());
            stmt.setDouble(5, obj.getTotalPrice());
            stmt.setDouble(6, obj.getDiscount());
            stmt.setDouble(7, obj.getNetPrice());
            stmt.setDouble(8, obj.getMoneyReceive());
            stmt.setDouble(9, obj.getMoneyChange());
            
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;    }

    @Override
    public Receipt update(Receipt obj) {
        String sql = "UPDATE RECEIPT"
                + " SET ingredient_name = ?,  ingredient_minNeed = ?, ingredient_quantity = ?, ingredient_value"
                + " WHERE ingredient_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getCustomer().getId());
            stmt.setInt(2, obj.getEmployee().getId());
            stmt.setString(3, obj.getBranch().getId());
            stmt.setDate(4, (Date) obj.getDate());
            stmt.setDouble(5, obj.getTotalPrice());
            stmt.setDouble(6, obj.getDiscount());
            stmt.setDouble(7, obj.getNetPrice());
            stmt.setDouble(8, obj.getMoneyReceive());
            stmt.setDouble(9, obj.getMoneyChange());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Receipt obj) {
        String sql = "DELETE FROM RECEIPT WHERE receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;  
    }

    @Override
    public List<Receipt> getAll(String where, String order) {
        Receipt rcdao = new Receipt();
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                rcdao = Receipt.fromRS(rs);
                list.add(rcdao);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }    
}

