
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class EmployeeDao implements Dao<Employee>{

    @Override
    public Employee get(int id) {
      Employee emp = new Employee();
        String sql = "SELECT * FROM EMPLOYEE WHERE employee_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                emp = emp.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return emp;
    }

    @Override
    public ArrayList<Employee> getAll() {
        Employee employee = new Employee();
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                employee = employee.fromRS(rs);
                list.add(employee);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Employee insert(Employee obj) {
        String sql = "INSERT INTO EMPLOYEE (branch_id, employee_title, employee_name, employee_lastname, employee_tel, employee_birthDate, employee_minWork, employee_qualification, employee_startDate, employee_moneyRate)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getBranch().getId());
            stmt.setString(2, obj.getTitle());
            stmt.setString(3, obj.getFirstName());
            stmt.setString(4, obj.getLastName());
            stmt.setString(5, obj.getTel());
            stmt.setString(6, obj.getBirthDate());
            stmt.setInt(7, obj.getMinWork());
            stmt.setString(8, obj.getQualification());
            stmt.setString(9, obj.getStartDate());
            stmt.setDouble(10, obj.getMoneyRate());
            
//            System.out.println(stmt);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }


    @Override
    public Employee update(Employee obj) {
        String sql = "UPDATE EMPLOYEE"
                + " SET branch_id = ?, employee_title = ?, employee_name = ?, employee_lastname = ?, employee_tel = ?,employee_birthDate = ?, employee_minWork = ?, employee_qualification = ?, employee_startDate = ?, employee_moneyRate = ?,"
                + " WHERE employee_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getBranch().getId());
            stmt.setString(2, obj.getTitle());
            stmt.setString(3, obj.getFirstName());
            stmt.setString(4, obj.getLastName());
            stmt.setString(5, obj.getTel());
            stmt.setString(6, obj.getBirthDate());
            stmt.setInt(7, obj.getMinWork());
            stmt.setString(8, obj.getQualification());
            stmt.setString(9, obj.getStartDate());
            stmt.setDouble(10, obj.getMoneyRate());
            stmt.setInt(5, obj.getId());

//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Employee obj) {
        String sql = "DELETE FROM EMPLOYEE WHERE employee_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;   
    }

    @Override
    public List<Employee> getAll(String where, String order) {

        Employee employee = new Employee();
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM EMPLOYEE ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                employee = employee.fromRS(rs);
                list.add(employee);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
}
