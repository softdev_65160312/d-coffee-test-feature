/*
 * Click nbfs://nbhost/SystemFileSystem/TcheckTimelates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/TcheckTimelates/Classes/Class.java to edit this tcheckTimelate
 */
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.CheckTime;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class CheckTimeDao implements Dao<CheckTime>{

    @Override
    public CheckTime get(int id) {
        CheckTime checkTime = new CheckTime();
        String sql = "SELECT * FROM CHECK_INOUT WHERE checkTime_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkTime = checkTime.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkTime;
    }

    @Override
    public List<CheckTime> getAll() {
        CheckTime checkTime = new CheckTime();
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_INOUT";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                checkTime = checkTime.fromRS(rs);
                list.add(checkTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;}

    @Override
    public CheckTime insert(CheckTime obj) {
        String sql = "INSERT INTO CHECK_INOUT (employee_id, checkTime_in, checkTime_out, checkTime_totalWorked, checkTime_type)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2, obj.getCheckIn());
            stmt.setString(3, obj.getCheckOut());
            stmt.setInt(4, obj.getTotalWorked());
            stmt.setString(5, obj.getType());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckTime update(CheckTime obj) {
        String sql = "UPDATE CHECK_INOUT"
                + " SET employee_id = ?, checkTime_in = ?, checkTime_out = ?, checkTime_totalWorked = ?, checkTime_type = ?"
                + " WHERE checkTime_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployee().getId());
            stmt.setString(2, obj.getCheckIn());
            stmt.setString(3, obj.getCheckOut());
            stmt.setInt(4, obj.getTotalWorked());
            stmt.setString(5, obj.getType());
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
    }
    }

    @Override
    public int delete(CheckTime obj) {
        String sql = "DELETE FROM CHECK_INOUT WHERE checkTime_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<CheckTime> getAll(String where, String order) {
        ArrayList<CheckTime> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_INOUT WHERE " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckTime checkTime = CheckTime.fromRS(rs);
                list.add(checkTime);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
        }
    }

    
    

