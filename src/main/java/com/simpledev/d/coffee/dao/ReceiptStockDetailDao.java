/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.dao;

import com.simpledev.d.coffee.helper.DatabaseHelper;
import com.simpledev.d.coffee.models.ReceiptStockDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class ReceiptStockDetailDao implements Dao<ReceiptStockDetail> {

    @Override
    public ReceiptStockDetail get(int id) {
        ReceiptStockDetail receiptStockDetail = new ReceiptStockDetail();
        String sql = "SELECT * FROM RECEIPT_STOCK_DETAIL WHERE receiptStockDetail_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receiptStockDetail = receiptStockDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receiptStockDetail;
    }

    @Override
    public List<ReceiptStockDetail> getAll() {
        ArrayList<ReceiptStockDetail> list = new ArrayList<>();
        String sql = "SELECT * FROM RECEIPT_STOCK_DETAIL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptStockDetail receiptStockDetail = new ReceiptStockDetail();
                list.add(receiptStockDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ReceiptStockDetail insert(ReceiptStockDetail obj) {
        String sql = "INSERT INTO RECEIPT_STOCK_DETAIL (receiptStock_id, ingrediant_id, receiptStockDetail_quantity, receiptStockDetail_description, receiptStockDetail_pricePerUnit, receiptStockDetail_discount, receiptStockDetail_netPrice)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setInt(1, obj.getId());
            stmt.setInt(2, obj.getQuantity());
            stmt.setString(3, obj.getDescription());
            stmt.setFloat(4, obj.getPricePerUnit());
            stmt.setFloat(5, obj.getDiscount());
            stmt.setFloat(6,obj.getNetPrice());

            stmt.setInt(1, obj.getReceiptStock().getId());
            stmt.setInt(2, obj.getIngrediant().getId());
            stmt.setInt(3, obj.getQuantity());
            stmt.setString(4, obj.getDescription());
            stmt.setFloat(5, obj.getPricePerUnit());
            stmt.setFloat(6, obj.getDiscount());
            stmt.setFloat(7,obj.getNetPrice());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ReceiptStockDetail update(ReceiptStockDetail obj) {
        String sql = "UPDATE RECEIPT_STOCK_DETAIL"
                + " SET receiptStock_id = ?, ingrediant_id = ?, receiptStockDetail_quantity = ?,  receiptStockDetail_description = ?, receiptStockDetail_pricePerUnit = ?, receiptStockDetail_discount = ?, receiptStockDetail_netPrice = ?"
                + " WHERE receiptStockDetail_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getReceiptStock().getId());
            stmt.setInt(2, obj.getIngrediant().getId());
            stmt.setInt(3, obj.getQuantity());
            stmt.setString(4, obj.getDescription());
            stmt.setFloat(5, obj.getPricePerUnit());
            stmt.setFloat(6, obj.getDiscount());
            stmt.setFloat(7,obj.getNetPrice());
            stmt.setInt(8,obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ReceiptStockDetail obj) {
        String sql = "DELETE FROM RECEIPT_STOCK_DETAIL WHERE receiptStockDetail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;  
    }

    @Override
    public List<ReceiptStockDetail> getAll(String where, String order) {
        ReceiptStockDetail receiptStockDetail;
        ArrayList<ReceiptStockDetail> list = new ArrayList();
        String sql = "SELECT * FROM RECEIPT_STOCK_DETAIL  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                receiptStockDetail = ReceiptStockDetail.fromRS(rs);
                list.add(receiptStockDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}