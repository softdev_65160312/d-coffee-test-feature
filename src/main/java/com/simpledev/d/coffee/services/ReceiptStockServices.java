/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.ReceiptStockDao;
import com.simpledev.d.coffee.models.ReceiptStock;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class ReceiptStockServices {
    public ArrayList<ReceiptStock> getAll() {
        ReceiptStockDao receiptStockDao = new ReceiptStockDao();
        return (ArrayList<ReceiptStock>) receiptStockDao.getAll();
    }
    public List<ReceiptStock> getUsersByOrder(String where, String order) {
        ReceiptStockDao receiptStockDao = new ReceiptStockDao();
        return receiptStockDao.getAll(where, order);
    }
    public void addNew(ReceiptStock editedStockDetail) {
       ReceiptStockDao receiptStockDao = new ReceiptStockDao();
       receiptStockDao.insert(editedStockDetail);
    }
    public void update(ReceiptStock editedStockDetail) {
        ReceiptStockDao receiptStockDao = new ReceiptStockDao();
        receiptStockDao.update(editedStockDetail);
    }
    public void delete(ReceiptStock editedStockDetail) {
        ReceiptStockDao receiptStockDao = new ReceiptStockDao();
        receiptStockDao.delete(editedStockDetail);
    }
}
