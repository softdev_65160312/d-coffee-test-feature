/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.SalaryPaymentDao;
import com.simpledev.d.coffee.models.SalaryPayment;
import java.util.List;

/**
 *
 * @author informatics
 */
public class SalaryPaymentService {
    public List<SalaryPayment> getAllPromotion() {
        SalaryPaymentDao salaryPayment = new SalaryPaymentDao();
        return salaryPayment.getAll();
    }
    
    public List<SalaryPayment> getSalaryPaymentsByOrder(String where, String order) {
        SalaryPaymentDao salaryPayment = new SalaryPaymentDao();
        return salaryPayment.getAll(where, order);
    }

    public void addNew(SalaryPayment editedSalaryPayment) {
        SalaryPaymentDao salaryPayment = new SalaryPaymentDao();
        salaryPayment.insert(editedSalaryPayment);
    }

    public void update(SalaryPayment editedSalaryPayment) {
        SalaryPaymentDao salaryPayment = new SalaryPaymentDao();
        salaryPayment.update(editedSalaryPayment);
    }

    public void delete(SalaryPayment editedSalaryPayment) {
        SalaryPaymentDao salaryPayment = new SalaryPaymentDao();
        salaryPayment.delete(editedSalaryPayment);
    }
}
