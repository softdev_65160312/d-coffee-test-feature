/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.ReceiptStockDetailDao;
import com.simpledev.d.coffee.models.ReceiptStockDetail;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class ReceiptStockDetailServices {
    public ArrayList<ReceiptStockDetail> getAll() {
        ReceiptStockDetailDao receiptStockDetailDao = new ReceiptStockDetailDao();
        return (ArrayList<ReceiptStockDetail>) receiptStockDetailDao.getAll();
    }
    public List<ReceiptStockDetail> getUsersByOrder(String where, String order) {
        ReceiptStockDetailDao receiptStockDetailDao = new ReceiptStockDetailDao();
        return receiptStockDetailDao.getAll(where, order);
    }
    public void addNew(ReceiptStockDetail editedStockDetail) {
       ReceiptStockDetailDao receiptStockDetailDao = new ReceiptStockDetailDao();
       receiptStockDetailDao.insert(editedStockDetail);
    }
    public void update(ReceiptStockDetail editedStockDetail) {
        ReceiptStockDetailDao receiptStockDetailDao = new ReceiptStockDetailDao();
        receiptStockDetailDao.update(editedStockDetail);
    }
    public void delete(ReceiptStockDetail editedStockDetail) {
        ReceiptStockDetailDao receiptStockDetailDao = new ReceiptStockDetailDao();
        receiptStockDetailDao.delete(editedStockDetail);
    }
}
