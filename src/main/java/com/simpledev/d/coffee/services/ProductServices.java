/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.ProductDao;
import com.simpledev.d.coffee.models.Product;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class ProductServices {
    public Product getProduct(int id) {
        ProductDao productDao = new ProductDao();
        return productDao.get(id);
    }
    
    public List<Product> getAll() {
        ProductDao productDao = new ProductDao();
        return productDao.getAll();
    }
    
    public void addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        productDao.insert(editedProduct);
    }

    public void update(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        productDao.update(editedProduct);
    }

    public void delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        productDao.delete(editedProduct);
    }}
