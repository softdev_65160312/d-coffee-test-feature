
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.EmployeeDao;
import com.simpledev.d.coffee.dao.EmployeeDao;
import com.simpledev.d.coffee.models.Employee;
import com.simpledev.d.coffee.models.Employee;
import java.util.ArrayList;
import java.util.List;


public class EmployeeServices {
    public ArrayList<Employee> getAll() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll();
    }
    public List<Employee> getUsersByOrder(String where, String order) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll(where, order);
    }
    public void addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        employeeDao.insert(editedEmployee);
    }
    public void update(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        employeeDao.update(editedEmployee);
    }
    public void delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        employeeDao.delete(editedEmployee);
    }

}
