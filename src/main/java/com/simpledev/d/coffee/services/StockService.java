/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.StockDao;
import com.simpledev.d.coffee.dao.UserDao;
import com.simpledev.d.coffee.models.Ingredient;
import com.simpledev.d.coffee.models.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class StockService {
    public ArrayList<Ingredient> getAll() {
        StockDao stockDao = new StockDao();
        return stockDao.getAll();
    }
    public List<Ingredient> getUsersByOrder(String where, String order) {
        StockDao stockDao = new StockDao();
        return stockDao.getAll(where, order);
    }
    public void addNew(Ingredient editedStock) {
        StockDao stockDao = new StockDao();
        stockDao.insert(editedStock);
    }
    public void update(Ingredient editedStock) {
        StockDao stockDao = new StockDao();
        stockDao.update(editedStock);
    }
    public void delete(Ingredient editedStock) {
        StockDao stockDao = new StockDao();
        stockDao.delete(editedStock);
    }

}
