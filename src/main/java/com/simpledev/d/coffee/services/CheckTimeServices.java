/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.CheckTimeDao;
import com.simpledev.d.coffee.models.CheckTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class CheckTimeServices {
    public ArrayList<CheckTime> getAll() {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        return (ArrayList<CheckTime>) checkTimeDao.getAll();
    }
    public List<CheckTime> getUsersByOrder(String where, String order) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        return checkTimeDao.getAll(where, order);
    }
    public void addNew(CheckTime editedStock) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        checkTimeDao.insert(editedStock);
    }
    public void update(CheckTime editedStock) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        checkTimeDao.update(editedStock);
    }
    public void delete(CheckTime editedStock) {
        CheckTimeDao checkTimeDao = new CheckTimeDao();
        checkTimeDao.delete(editedStock);
    }
}
