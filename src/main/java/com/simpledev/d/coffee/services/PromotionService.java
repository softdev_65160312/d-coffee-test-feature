/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.PromotionDao;
import com.simpledev.d.coffee.models.Promotion;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class PromotionService {

    public List<Promotion> getAllPromotions() {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getAll();
    }
    
    public List<Promotion> getPromotionsByOrder(String where, String order) {
        PromotionDao promotionDao = new PromotionDao();
        return promotionDao.getAll(where, order);
    }

    public void addNew(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        promotionDao.insert(editedPromotion);
    }

    public void update(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        promotionDao.update(editedPromotion);
    }

    public void delete(Promotion editedPromotion) {
        PromotionDao promotionDao = new PromotionDao();
        promotionDao.delete(editedPromotion);
    }
}
