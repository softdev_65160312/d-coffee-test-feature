/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.ReceiptDao;
import com.simpledev.d.coffee.models.Receipt;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class ReceiptService {
    public ArrayList<Receipt> getAll() {
        ReceiptDao receiptStockDetailDao = new ReceiptDao();
        return (ArrayList<Receipt>) receiptStockDetailDao.getAll();
    }
    public List<Receipt> getUsersByOrder(String where, String order) {
        ReceiptDao receiptStockDetailDao = new ReceiptDao();
        return receiptStockDetailDao.getAll(where, order);
    }
    public void addNew(Receipt editedStockDetail) {
       ReceiptDao receiptStockDetailDao = new ReceiptDao();
       receiptStockDetailDao.insert(editedStockDetail);
    }
    public void update(Receipt editedStockDetail) {
        ReceiptDao receiptStockDetailDao = new ReceiptDao();
        receiptStockDetailDao.update(editedStockDetail);
    }
    public void delete(Receipt editedStockDetail) {
        ReceiptDao receiptStockDetailDao = new ReceiptDao();
        receiptStockDetailDao.delete(editedStockDetail);
    }
}
