
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.CustomerDao;
import com.simpledev.d.coffee.dao.CustomerDao;
import com.simpledev.d.coffee.models.Customer;
import com.simpledev.d.coffee.models.Customer;
import java.util.ArrayList;
import java.util.List;


public class CustomerServices {
    public ArrayList<Customer> getAll() {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.getAll();
    }
    public List<Customer> getUsersByOrder(String where, String order) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.getAll(where, order);
    }
    public void addNew(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        customerDao.insert(editedCustomer);
    }
    public void update(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        customerDao.update(editedCustomer);
    }
    public void delete(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        customerDao.delete(editedCustomer);
    }

}
