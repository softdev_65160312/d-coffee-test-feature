/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.ReceiptDetailDao;
import com.simpledev.d.coffee.models.ReceiptDetail;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class ReceiptDetailServices {
    public ArrayList<ReceiptDetail> getAll() {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return (ArrayList<ReceiptDetail>) receiptDetailDao.getAll();
    }
    public List<ReceiptDetail> getUsersByOrder(String where, String order) {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        return receiptDetailDao.getAll(where, order);
    }
    public void addNew(ReceiptDetail editedStockDetail) {
       ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
       receiptDetailDao.insert(editedStockDetail);
    }
    public void update(ReceiptDetail editedStockDetail) {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        receiptDetailDao.update(editedStockDetail);
    }
    public void delete(ReceiptDetail editedStockDetail) {
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        receiptDetailDao.delete(editedStockDetail);
    }
}
