/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.services;

import com.simpledev.d.coffee.dao.CheckStockDao;
import com.simpledev.d.coffee.models.CheckStock;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class CheckStockServices {
    public ArrayList<CheckStock> getAll() {
        CheckStockDao chStockDao = new CheckStockDao();
        return (ArrayList<CheckStock>) chStockDao.getAll();
    }
    public List<CheckStock> getUsersByOrder(String where, String order) {
        CheckStockDao chStockDao = new CheckStockDao();
        return chStockDao.getAll(where, order);
    }
    public void addNew(CheckStock editedStock) {
        CheckStockDao chStockDao = new CheckStockDao();
        chStockDao.insert(editedStock);
    }
    public void update(CheckStock editedStock) {
        CheckStockDao chStockDao = new CheckStockDao();
        chStockDao.update(editedStock);
    }
    public void delete(CheckStock editedStock) {
        CheckStockDao chStockDao = new CheckStockDao();
        chStockDao.delete(editedStock);
    }
}
