package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.BranchDao;
import com.simpledev.d.coffee.dao.CustomerDao;
import com.simpledev.d.coffee.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Receipt {

    private int id;
    private Customer customer;
    private Employee employee;
    private Branch branch;
    private Date date;
    private double totalPrice;
    private double discount;
    private double netPrice;
    private double moneyReceive;
    private double moneyChange;
    private ArrayList<ReceiptDetail> receiptDetails;

    public Receipt(Customer customer, Employee employee, Branch branch, Date date, double totalPrice, double discount, double netPrice, double moneyReceive, double moneyChange, ArrayList<ReceiptDetail> receiptDetails) {
        this.id = -1;
        this.customer = customer;
        this.employee = employee;
        this.branch = branch;
        this.date = date;
        this.totalPrice = totalPrice;
        this.discount = discount;
        this.netPrice = netPrice;
        this.moneyReceive = moneyReceive;
        this.moneyChange = moneyChange;
        this.receiptDetails = receiptDetails;
    }

    public Receipt() {
        this.id = -1;
        this.customer = null;
        this.employee = null;
        this.branch = null;
        this.date = null;
        this.totalPrice = 0;
        this.discount = 0;
        this.netPrice = 0;
        this.moneyReceive = 0;
        this.moneyChange = 0;
        this.receiptDetails = new ArrayList();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(double netPrice) {
        this.netPrice = netPrice;
    }

    public double getMoneyReceive() {
        return moneyReceive;
    }

    public void setMoneyReceive(double moneyReceive) {
        this.moneyReceive = moneyReceive;
    }

    public double getMoneyChange() {
        return moneyChange;
    }

    public void setMoneyChange(double moneyChange) {
        this.moneyChange = moneyChange;
    }

    public void setRecieptDetails(ArrayList<ReceiptDetail> recieptDetails) {
        this.receiptDetails = recieptDetails;
    }

    public void addReceiptDetail(ReceiptDetail receiptDetail) {
        receiptDetails.add(receiptDetail);
        calculateTotal();
    }

    public void delReceoptDetail(ReceiptDetail receiptDetail) {
        receiptDetails.remove(receiptDetail);
        calculateTotal();
    }

    private void calculateTotal() {
        int qty_total = 0;
        float total = 0.0f;
        for (ReceiptDetail rd : receiptDetails) {
            total += rd.getUnit() * rd.getUnitprice();
            qty_total += rd.getUnit();
        }
//        this.totalQty = qty_total;
        this.totalPrice = total;
    }
//
//        

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", customer=" + customer + ", employee=" + employee + ", branch=" + branch + ", date=" + date + ", totalPrice=" + totalPrice + ", discount=" + discount + ", netPrice=" + netPrice + ", moneyReceive=" + moneyReceive + ", moneyChange=" + moneyChange + ", receiptDetails=" + receiptDetails + '}';
    }
    
    
    public static Receipt fromRS(ResultSet rs) {
        // create new object standby
        Receipt receipt = new Receipt();
        // import needed DAO
        CustomerDao customerDao = new CustomerDao();
        EmployeeDao employeeDao = new EmployeeDao();
        BranchDao branchDao = new BranchDao();
        // local variable
        Date date;
        int cus_id;
        int emp_id;
        String bra_code;
        try {
            // format date to "year/month/day"
            DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
            date = df.parse(rs.getString("receipt_date"));
            // get id
            cus_id = (rs.getInt("customer_id"));
            emp_id = (rs.getInt("employee_id"));
            bra_code = (rs.getString("branch_code"));
            //  set object
            receipt.setId(rs.getInt("receipt_id"));
            receipt.setTotalPrice(rs.getDouble("receipt_totalPrice"));
            receipt.setDiscount(rs.getDouble("receipt_discount"));
            receipt.setNetPrice(rs.getDouble("receipt_netPrice"));
            receipt.setMoneyReceive(rs.getDouble("receipt_moneyReceive"));
            receipt.setMoneyChange(rs.getFloat("receipt_moneyChange"));
            receipt.setCustomer(customerDao.get(cus_id));
            receipt.setEmployee(employeeDao.get(emp_id));
            receipt.setBranch(branchDao.get(bra_code));
            receipt.setDate(date);

        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
        }
        return receipt;
    }

}
