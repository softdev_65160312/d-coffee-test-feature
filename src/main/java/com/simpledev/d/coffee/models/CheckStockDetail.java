/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.CheckStockDao;
import com.simpledev.d.coffee.dao.StockDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sirikon
 */
public class CheckStockDetail {

    private int id;
    private Ingredient stock;
    private CheckStock checkStock;
    private String description;
    private int quantity;
    private int unitExpire;
    private int valueLoss;
    private String date;

    public CheckStockDetail(Ingredient stock, CheckStock checkStock, String description, int quantity, int unitExpire, int valueLoss, String date) {
        this.id = -1;
        this.stock = stock;
        this.checkStock = checkStock;
        this.description = description;
        this.quantity = quantity;
        this.unitExpire = unitExpire;
        this.valueLoss = valueLoss;
        this.date = date;
    }

    public CheckStockDetail() {
        this.id = -1;
        this.stock = null;
        this.checkStock = null;
        this.description = "";
        this.quantity = 0;
        this.unitExpire = 0;
        this.valueLoss = 0;
        this.date = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Ingredient getStock() {
        return stock;
    }

    public void setStock(Ingredient stock) {
        this.stock = stock;
    }

    public CheckStock getCheckStock() {
        return checkStock;
    }

    public void setCheckStock(CheckStock checkStock) {
        this.checkStock = checkStock;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getUnitExpire() {
        return unitExpire;
    }

    public void setUnitExpire(int unitExpire) {
        this.unitExpire = unitExpire;
    }

    public int getValueLoss() {
        return valueLoss;
    }

    public void setValueLoss(int valueLoss) {
        this.valueLoss = valueLoss;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "CheckStockDetail{" + "id=" + id + ", stock=" + stock + ", checkStock=" + checkStock + ", description=" + description + ", quantity=" + quantity + ", unitExpire=" + unitExpire + ", valueLoss=" + valueLoss + ", date=" + date + '}';
    }

    public static CheckStockDetail fromRS(ResultSet rs) {
        CheckStockDetail chStockDetail = new CheckStockDetail();
        CheckStockDao checkstockDao = new CheckStockDao();
        StockDao stockDao = new StockDao();
        int stock_id;
        int chStock_id;
        try {
            // get only id
            stock_id = (rs.getInt("stock_id"));
            chStock_id = (rs.getInt("checkStock_id"));
            // set object
            chStockDetail.setId(rs.getInt("checkStockDetail_id"));
            chStockDetail.setDescription(rs.getString("checkStockDetail_description"));
            chStockDetail.setQuantity(rs.getInt("checkStockDetail_quantity"));
            chStockDetail.setUnitExpire(rs.getInt("checkStockDetail_unitExpire"));
            chStockDetail.setValueLoss(rs.getInt("checkStockDetail_valueLoss"));
            chStockDetail.setDate(rs.getString("checkStockDetail_date"));
            chStockDetail.setStock(stockDao.get(stock_id));
            chStockDetail.setCheckStock(checkstockDao.get(chStock_id));
        } catch (SQLException ex) {
            Logger.getLogger(CheckStockDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return chStockDetail;
    }
}
