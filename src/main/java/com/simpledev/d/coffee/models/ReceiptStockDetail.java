/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.ReceiptStockDao;
import com.simpledev.d.coffee.dao.StockDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class ReceiptStockDetail {

    private int id;
    private ReceiptStock receiptStock;
    private Ingredient ingredient;
    private int quantity;
    private String description;
    private float pricePerUnit;
    private float discount;
    private float netPrice;

    public ReceiptStockDetail(int id, ReceiptStock receiptStockid, Ingredient ingrediantcode, int quantity, String description, float pricePerUnit, float discount, float netPrice) {
        this.id = -1;
        this.receiptStock = receiptStockid;
        this.ingredient = ingrediantcode;
        this.quantity = quantity;
        this.description = description;
        this.pricePerUnit = pricePerUnit;
        this.discount = discount;
        this.netPrice = netPrice;
    }

    public ReceiptStockDetail() {
        this.id = -1;
        this.receiptStock = null;
        this.ingredient = null;
        this.quantity = 0;
        this.description = "";
        this.pricePerUnit = 0;
        this.discount = 0;
        this.netPrice = 0;
    }

    public ReceiptStock getReceiptStock() {
        return receiptStock;
    }

    public void setReceiptStock(ReceiptStock receiptStock) {
        this.receiptStock = receiptStock;
    }

    public Ingredient getIngrediant() {
        return ingredient;
    }

    public void setIngrediant(Ingredient ingrediant) {
        this.ingredient = ingrediant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(float pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(float netPrice) {
        this.netPrice = netPrice;
    }

    @Override
    public String toString() {
        return "ReceiptStockDetail{" + "id=" + id + ", receiptStockid=" + receiptStock + ", ingrediantcode=" + ingredient + ", quantity=" + quantity + ", description=" + description + ", pricePerUnit=" + pricePerUnit + ", discount=" + discount + ", netPrice=" + netPrice + '}';
    }

    public static ReceiptStockDetail fromRS(ResultSet rs) {
        ReceiptStockDetail receiptStockDetail = new ReceiptStockDetail();
        ReceiptStockDao receiptStockDao = new ReceiptStockDao();
        StockDao stockDao = new StockDao();
        int receipt_id;
        int ind_id;
        try {
            // get id
            receipt_id = (rs.getInt("receiptStock_id"));
            ind_id = (rs.getInt("ingredient_id"));
            // set object
            receiptStockDetail.setId(rs.getInt("receiptStockDetail_id"));
            receiptStockDetail.setQuantity(rs.getInt("receiptStockDetail_quantity"));
            receiptStockDetail.setDescription(rs.getString("receiptStockDetail_description"));
            receiptStockDetail.setPricePerUnit(rs.getFloat("receiptStockDetail_pricePerUnit"));
            receiptStockDetail.setDiscount(rs.getFloat("receiptStockDetail_discount"));
            receiptStockDetail.setNetPrice(rs.getFloat("receiptStockDetail_netPrice"));
            receiptStockDetail.setReceiptStock(receiptStockDao.get(receipt_id));
            receiptStockDetail.setIngrediant(stockDao.get(ind_id));

        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receiptStockDetail;
    }
}
