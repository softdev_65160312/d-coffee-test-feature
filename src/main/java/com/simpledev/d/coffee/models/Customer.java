/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sirikon
 */
public class Customer {
    private int id;
    private String state;
    private String firstName;
    private String lastName;
    private String tel;
    private String birthDate;
    private int point;
    private String joindate;

    public Customer(int id, String state, String firstName, String lastName, String tel, String birthDate,  int point, String joindate) {
        this.id = -1;
        this.state = state;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tel = tel;
        this.birthDate = birthDate;
        this.point = point;
        this.joindate = joindate;
    }

    public Customer(){
        this.id = -1;
        this.state = "";
        this.firstName = "";
        this.lastName = "";
        this.tel = "";
        this.birthDate = "";   
        this.point = 0;
        this.joindate = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getJoindate() {
        return joindate;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", state=" + state + ", firstName=" + firstName + ", lastName=" + lastName + ", tel=" + tel + ", birthDate=" + birthDate + ", point=" + point + ", joindate=" + joindate + '}';
    }
    
    public void setJoindate(String joindate) {
        this.joindate = joindate;
    }
        public Customer fromRS(ResultSet rs) {
        Customer cus = new Customer();
        try {
            cus.setId(rs.getInt("customer_id"));
            cus.setState(rs.getString("customer_status"));
            cus.setFirstName(rs.getString("customer_name"));
            cus.setLastName(rs.getString("customer_lastname"));
            cus.setTel(rs.getString("customer_tel"));
            cus.setBirthDate(rs.getString("customer_birth_date"));
            cus.setPoint(rs.getInt("customer_point"));
            cus.setJoindate(rs.getString("customer_join_date"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return cus;
    }
}
