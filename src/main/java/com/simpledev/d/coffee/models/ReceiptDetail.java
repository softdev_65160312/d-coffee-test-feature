package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.ProductDao;
import com.simpledev.d.coffee.dao.PromotionDao;
import com.simpledev.d.coffee.dao.PromotionDetailDao;
import com.simpledev.d.coffee.dao.ReceiptDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReceiptDetail {

    private int id;
    private Product product;
    private PromotionDetail promotionDetail;
    private Receipt receipt;
    private int unit;
    private double unitprice;
    private double discount;
    private double netPrice;

    public ReceiptDetail(Product product, PromotionDetail promotionDetail, Receipt receipt, int unit, double unitprice, double discount, double netPrice) {
        this.id = -1;
        this.product = product;
        this.promotionDetail = promotionDetail;
        this.receipt = receipt;
        this.unit = unit;
        this.unitprice = unitprice;
        this.discount = discount;
        this.netPrice = netPrice;
    }

    public ReceiptDetail() {
        this.id = -1;
        this.product = null;
        this.promotionDetail = null;
        this.receipt = null;
        this.unit = 0;
        this.unitprice = 0;
        this.discount = 0;
        this.netPrice = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public PromotionDetail getPromotionDetail() {
        return promotionDetail;
    }

    public void setPromotionDetail(PromotionDetail promotionDetail) {
        this.promotionDetail = promotionDetail;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(double netPrice) {
        this.netPrice = netPrice;
    }

    public static ReceiptDetail fromRS(ResultSet rs) {
        ReceiptDetail receiptDetail = new ReceiptDetail();
        ProductDao productDao = new ProductDao();
        PromotionDetailDao promotionDetailDao = new PromotionDetailDao();
        ReceiptDao receiptDao = new ReceiptDao();
        int product_id;
        int promotionDetail_id;
        int receipt_id;

        try {
            // get only id for set object by check that id
            product_id = rs.getInt("product_id");
            promotionDetail_id = rs.getInt("promotionDetail_id");
            receipt_id = rs.getInt("receipt_id");
            // set object
            receiptDetail.setId(rs.getInt("receiptDetail_id"));
            receiptDetail.setUnit(rs.getInt("receiptDetail_unit"));
            receiptDetail.setUnitprice(rs.getFloat("receiptDetail_unitPrice"));
            receiptDetail.setDiscount(rs.getFloat("receiptDetail_discount"));
            receiptDetail.setNetPrice(rs.getFloat("receiptDetail_netPrice"));
            receiptDetail.setProduct(productDao.get(product_id));
            receiptDetail.setPromotionDetail(promotionDetailDao.get(promotionDetail_id));
            receiptDetail.setReceipt(receiptDao.get(receipt_id));
        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receiptDetail;
    }

}
