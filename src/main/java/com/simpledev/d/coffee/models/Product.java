package com.simpledev.d.coffee.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Product {

    private int id;
    private String name;
    private double price;
    private String size;
    private String category;
    private String subCategory;

    public Product(int id, String name, double price, String size, int quantity, String category, String subCategory) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.size = size;
        this.category = category;
        this.subCategory = subCategory;
    }

    public Product(String name, double price, String size, String category, String subCategory) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.size = size;
        this.category = category;
        this.subCategory = subCategory;
    }

    public Product() {
        this.id = -1;
        this.name = "";
        this.price = 0;
        this.size = "";
        this.category = "";
        this.subCategory = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    @Override
    public String toString() {
        return "Product{" + "code=" + id + ", name=" + name + ", price=" + price + ", size=" + size + ", category=" + category + ", subCategory=" + subCategory + '}';
    }

    public static Product fromRS(ResultSet rs) {
        Product pd = new Product();
        try {
            pd.setId(rs.getInt("product_id"));
            pd.setName(rs.getString("product_name"));
            pd.setPrice(rs.getDouble("product_price"));
            pd.setSize(rs.getString("product_size"));
            pd.setCategory(rs.getString("product_category"));
            pd.setSubCategory(rs.getString("product_subCategory"));
            return pd;

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
