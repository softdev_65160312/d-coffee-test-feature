/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.simpledev.d.coffee.models;

import java.sql.ResultSet;

/**
 *
 * @author informatics
 */
public class Vendor {
    private String code;
    private String name;
    private String contact;
    private String locate;
    private String tel;
    private String state;
    private int orderCount;

    public Vendor(String code, String name, String contact, String locate, String tel, String state, int orderCount) {
        this.code = code;
        this.name = name;
        this.contact = contact;
        this.locate = locate;
        this.tel = tel;
        this.state = state;
        this.orderCount = orderCount;
    }

    public Vendor() {
        this.code = "";
        this.name = "";
        this.contact = "";
        this.locate = "";
        this.tel = "";
        this.state = "";
        this.orderCount = 0;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getLocate() {
        return locate;
    }

    public void setLocate(String locate) {
        this.locate = locate;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    @Override
    public String toString() {
        return "Vendor{" + "code=" + code + ", name=" + name + ", contact=" + contact + ", locate=" + locate + ", tel=" + tel + ", state=" + state + ", orderCount=" + orderCount + '}';
    }
    
    public static Vendor fromRS(ResultSet rs) {
        Vendor vendor = new Vendor();
        return null;
    }
}
