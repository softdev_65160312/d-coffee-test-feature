package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.BranchDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Employee {

    private int id;
    private Branch branch;
    private String title;
    private String firstName;
    private String lastName;
    private String tel;
    private String birthDate;
    private int minWork;
    private String qualification;
    private String startDate;
    private double moneyRate;

    public Employee(int id, Branch branch, String title, String firstName, String lastName, String tel, String birthDate, int minWork, String qualification, String startDate, double moneyRate) {
        this.id = id;
        this.branch = branch;
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tel = tel;
        this.birthDate = birthDate;
        this.minWork = minWork;
        this.qualification = qualification;
        this.startDate = startDate;
        this.moneyRate = moneyRate;
    }

    public Employee(Branch branch, String title, String firstName, String lastName, String tel, String birthDate, int minWork, String qualification, String startDate, double moneyRate) {
        this.id = -1;
        this.branch = branch;
        this.title = title;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tel = tel;
        this.birthDate = birthDate;
        this.minWork = minWork;
        this.qualification = qualification;
        this.startDate = startDate;
        this.moneyRate = moneyRate;
    }

    public Employee() {
        this.id = -1;
        this.branch = null;
        this.title = "";
        this.firstName = "";
        this.lastName = "";
        this.tel = "";
        this.birthDate = "";
        this.minWork = 0;
        this.qualification = "";
        this.startDate = "";
        this.moneyRate = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public int getMinWork() {
        return minWork;
    }

    public void setMinWork(int minWork) {
        this.minWork = minWork;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public double getMoneyRate() {
        return moneyRate;
    }

    public void setMoneyRate(double moneyRate) {
        this.moneyRate = moneyRate;
    }


    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", branch=" + branch + ", title=" + title + ", firstName=" + firstName + ", lastName=" + lastName + ", tel=" + tel + ", birthDate=" + birthDate + ", minWork=" + minWork + ", qualification=" + qualification + ", startDate=" + startDate + ", moneyRate=" + moneyRate + '}';
    }

    
    
    public static Employee fromRS(ResultSet rs) {
        Employee emp = new Employee();
        BranchDao branchDao = new BranchDao();
        String branch_id;
        try {
            emp.setId(rs.getInt("employee_id"));
            branch_id = (rs.getString("branch_id"));

            emp.setTitle(rs.getString("employee_title"));
            emp.setFirstName(rs.getString("employee_name"));
            emp.setLastName(rs.getString("employee_lastname"));
            emp.setTel(rs.getString("employee_tel"));
            emp.setBirthDate(rs.getString("employee_birthDate"));
            emp.setMinWork(rs.getInt("employee_minWork"));
            emp.setQualification(rs.getString("employee_qualification"));
            emp.setStartDate(rs.getString("employee_startDate"));
            emp.setMoneyRate(rs.getDouble("employee_moneyRate"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return emp;
    }
}

