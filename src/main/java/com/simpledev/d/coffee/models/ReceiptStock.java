/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpledev.d.coffee.models;

import com.simpledev.d.coffee.dao.EmployeeDao;
import com.simpledev.d.coffee.dao.VendorDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class ReceiptStock {
    private int id;
    private Employee employee;
    private Vendor vendor;
    private int quantity;
    private float total;
    private float discount;
    private float netPrice;
    private String dateOrder;
    private String dateReceive;
    private String datePaid;
    private float change;
    private String status;

    public ReceiptStock(int id, Employee employee, Vendor vendor, int quantity, float total, float discount, float netPrice, String dateOrder, String dateReceive, String datePaid, float change, String status) {
        this.id = id;
        this.employee = employee;
        this.vendor = vendor;
        this.quantity = quantity;
        this.total = total;
        this.discount = discount;
        this.netPrice = netPrice;
        this.dateOrder = dateOrder;
        this.dateReceive = dateReceive;
        this.datePaid = datePaid;
        this.change = change;
        this.status = status;
    }

    public ReceiptStock(Employee employee, Vendor vendor, int quantity, float total, float discount, float netPrice, String dateOrder, String dateReceive, String datePaid, float change, String status) {
        this.id = -1;
        this.employee = employee;
        this.vendor = vendor;
        this.quantity = quantity;
        this.total = total;
        this.discount = discount;
        this.netPrice = netPrice;
        this.dateOrder = dateOrder;
        this.dateReceive = dateReceive;
        this.datePaid = datePaid;
        this.change = change;
        this.status = status;
    }
        
    public ReceiptStock() {
        this.id = -1;
        this.employee = null;
        this.vendor = null;
        this.quantity = 0;
        this.total = 0;
        this.discount = 0;
        this.netPrice = 0;
        this.dateOrder = "";
        this.dateReceive = "";
        this.datePaid = "";
        this.change = 0;
        this.status = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(float netPrice) {
        this.netPrice = netPrice;
    }

    public String getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(String dateOrder) {
        this.dateOrder = dateOrder;
    }

    public String getDateReceive() {
        return dateReceive;
    }

    public void setDateReceive(String dateReceive) {
        this.dateReceive = dateReceive;
    }

    public String getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(String datePaid) {
        this.datePaid = datePaid;
    }

    public float getChange() {
        return change;
    }

    public void setChange(float change) {
        this.change = change;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ReceiptStock{" + "id=" + id + ", employee=" + employee + ", vendor=" + vendor + ", quantity=" + quantity + ", total=" + total + ", discount=" + discount + ", netPrice=" + netPrice + ", dateOrder=" + dateOrder + ", dateReceive=" + dateReceive + ", datePaid=" + datePaid + ", change=" + change + ", status=" + status + '}';
    }
    
    public static ReceiptStock fromRS(ResultSet rs) {
        ReceiptStock receiptStock = new ReceiptStock();
        EmployeeDao employeeDao = new EmployeeDao();
        VendorDao vendorDao = new VendorDao();
        int emp_id;
        int vd_id;
        try {
            receiptStock.setId(rs.getInt("receiptStock_id"));
            emp_id = (rs.getInt("employee_id"));
            vd_id = (rs.getInt("vendor_code"));
            receiptStock.setTotal(rs.getFloat("receiptStock_total"));
            receiptStock.setDiscount(rs.getFloat("receiptStock_discount"));
            receiptStock.setNetPrice(rs.getFloat("receiptStock_netPrice"));
            receiptStock.setDateOrder(rs.getString("receiptStock_dateOrder")); 
            receiptStock.setDateReceive(rs.getString("receiptStock_dateReceive")); 
            receiptStock.setDatePaid(rs.getString("receiptStock_datePaid"));
            receiptStock.setChange(rs.getFloat("receiptStock_change"));
            receiptStock.setStatus(rs.getString("receiptStock_status"));
            
            

        } catch (SQLException ex) {
            Logger.getLogger(ReceiptStock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receiptStock;
    }
    
}
