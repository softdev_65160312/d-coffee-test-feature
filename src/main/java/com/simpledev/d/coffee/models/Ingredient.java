package com.simpledev.d.coffee.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ingredient {

    private int id;
    private String name;
    private int minNeed;
    private int quantity;
    private double value;

    public Ingredient(int id, String name, int minNeed, int quantity, double value) {
        this.id = id;
        this.name = name;
        this.minNeed = minNeed;
        this.quantity = quantity;
        this.value = value;
    }

    public Ingredient() {
        this.id = -1;
        this.name = "";
        this.minNeed = 0;
        this.quantity = 0;
        this.value = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

      
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinNeed() {
        return minNeed;
    }

    public void setMinNeed(int minNeed) {
        this.minNeed = minNeed;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Ingredient{" + "code=" + id + ", name=" + name + ", minNeed=" + minNeed + ", quantity=" + quantity + ", value=" + value + '}';
    }

    public static Ingredient fromRS(ResultSet rs) {
        Ingredient igd = new Ingredient();
        try {
            igd.setId(rs.getInt("ingredient_id"));
            igd.setName(rs.getString("ingredient_name"));
            igd.setMinNeed(rs.getInt("ingredient_minNeed"));
            igd.setQuantity(rs.getInt("ingredient_quantity"));
            igd.setValue(rs.getDouble("ingredient_value"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return igd;
    }

}
