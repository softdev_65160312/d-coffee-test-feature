package com.simpledev.d.coffee.common;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommonFont {

    public static Font getFont() {
        try {
            Font customFont = java.awt.Font.createFont(Font.TRUETYPE_FONT, new File("font/db_adman_x.ttf"));
            return customFont.deriveFont(java.awt.Font.PLAIN, 16); // Change font style and size as needed
        } catch (FontFormatException ex) {
            Logger.getLogger(CommonFont.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CommonFont.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
