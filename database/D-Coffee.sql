--
-- File generated with SQLiteStudio v3.4.4 on พฤ. ต.ค. 5 17:39:28 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: BRANCH
DROP TABLE IF EXISTS BRANCH;

CREATE TABLE IF NOT EXISTS BRANCH (
    branch_id     TEXT NOT NULL
                       UNIQUE,
    branch_locate TEXT NOT NULL,
    branch_tel    TEXT NOT NULL,
    PRIMARY KEY (
        branch_id
    )
);

INSERT INTO BRANCH (
                       branch_id,
                       branch_locate,
                       branch_tel
                   )
                   VALUES (
                       'BUU',
                       'somewhere in chonburi',
                       '02212224'
                   );

INSERT INTO BRANCH (
                       branch_id,
                       branch_locate,
                       branch_tel
                   )
                   VALUES (
                       'CENTRAL',
                       'near central chonburi',
                       '02312345'
                   );

INSERT INTO BRANCH (
                       branch_id,
                       branch_locate,
                       branch_tel
                   )
                   VALUES (
                       'SIAMSQUARE',
                       'near siam square one',
                       '02412456'
                   );


-- Table: CHECK_INOUT
DROP TABLE IF EXISTS CHECK_INOUT;

CREATE TABLE IF NOT EXISTS CHECK_INOUT (
    checkTime_id          INTEGER NOT NULL
                                  UNIQUE,
    checkTime_date        TEXT    NOT NULL,
    employee_id           INTEGER NOT NULL,
    checkTime_in          TEXT    NOT NULL,
    checkTime_out         TEXT    NOT NULL,
    checkTime_totalWorked INTEGER NOT NULL,
    checkTime_type        TEXT    NOT NULL,
    PRIMARY KEY (
        checkTime_id
    ),
    FOREIGN KEY (
        employee_id
    )
    REFERENCES EMPLOYEE
);

INSERT INTO CHECK_INOUT (
                            checkTime_id,
                            checkTime_date,
                            employee_id,
                            checkTime_in,
                            checkTime_out,
                            checkTime_totalWorked,
                            checkTime_type
                        )
                        VALUES (
                            1,
                            '2021/02/03',
                            1,
                            '10:00:00',
                            '18:00:00',
                            8,
                            'normal'
                        );

INSERT INTO CHECK_INOUT (
                            checkTime_id,
                            checkTime_date,
                            employee_id,
                            checkTime_in,
                            checkTime_out,
                            checkTime_totalWorked,
                            checkTime_type
                        )
                        VALUES (
                            2,
                            '2021/02/03',
                            2,
                            '10:00:00',
                            '16:00:00',
                            6,
                            'normal'
                        );

INSERT INTO CHECK_INOUT (
                            checkTime_id,
                            checkTime_date,
                            employee_id,
                            checkTime_in,
                            checkTime_out,
                            checkTime_totalWorked,
                            checkTime_type
                        )
                        VALUES (
                            3,
                            '2021/02/03',
                            3,
                            '10:00:00',
                            '18:00:00',
                            8,
                            'normal'
                        );


-- Table: CHECK_STOCK
DROP TABLE IF EXISTS CHECK_STOCK;

CREATE TABLE IF NOT EXISTS CHECK_STOCK (
    checkStock_id          INTEGER NOT NULL
                                   UNIQUE,
    employee_id            INTEGER NOT NULL,
    checkStock_description TEXT    NOT NULL,
    checkStock_valueLoss   INTEGER,
    checkStock_unitLoss    INTEGER,
    checkStock_totalUnit   INTEGER NOT NULL,
    checkStock_date        TEXT    NOT NULL,
    PRIMARY KEY (
        checkStock_id
    ),
    FOREIGN KEY (
        employee_id
    )
    REFERENCES EMPLOYEE
);

INSERT INTO CHECK_STOCK (
                            checkStock_id,
                            employee_id,
                            checkStock_description,
                            checkStock_valueLoss,
                            checkStock_unitLoss,
                            checkStock_totalUnit,
                            checkStock_date
                        )
                        VALUES (
                            1,
                            1,
                            'ผงชาเขียว',
                            5,
                            5,
                            10,
                            '2021/02/03'
                        );

INSERT INTO CHECK_STOCK (
                            checkStock_id,
                            employee_id,
                            checkStock_description,
                            checkStock_valueLoss,
                            checkStock_unitLoss,
                            checkStock_totalUnit,
                            checkStock_date
                        )
                        VALUES (
                            2,
                            2,
                            'เมล็ดกาแฟ',
                            5,
                            5,
                            10,
                            '2021/02/03'
                        );


-- Table: CHECK_STOCK_DETAIL
DROP TABLE IF EXISTS CHECK_STOCK_DETAIL;

CREATE TABLE IF NOT EXISTS CHECK_STOCK_DETAIL (
    checkStockDetail_id          INTEGER NOT NULL
                                         UNIQUE,
    ingrediant_code              TEXT    NOT NULL
                                         REFERENCES INGREDIENT (ingredient_code),
    checkStock_id                TEXT    NOT NULL,
    checkStockDetail_description TEXT    NOT NULL,
    checkStockDetail_quantity    INTEGER NOT NULL,
    checkStockDetail_unitExpire  INTEGER NOT NULL,
    checkStockDetail_valueLoss   INTEGER NOT NULL,
    checkStockDetail_date        TEXT,
    PRIMARY KEY (
        checkStockDetail_id
    ),
    FOREIGN KEY (
        ingrediant_code
    )
    REFERENCES INGREDIENT (ingredient_code),
    FOREIGN KEY (
        checkStock_id
    )
    REFERENCES CHECK_STOCK
);

INSERT INTO CHECK_STOCK_DETAIL (
                                   checkStockDetail_id,
                                   ingrediant_code,
                                   checkStock_id,
                                   checkStockDetail_description,
                                   checkStockDetail_quantity,
                                   checkStockDetail_unitExpire,
                                   checkStockDetail_valueLoss,
                                   checkStockDetail_date
                               )
                               VALUES (
                                   1,
                                   '1',
                                   '1',
                                   'ผงชาเขียวหมดไป 5 ถุง',
                                   5,
                                   0,
                                   5,
                                   '2021/02/03'
                               );

INSERT INTO CHECK_STOCK_DETAIL (
                                   checkStockDetail_id,
                                   ingrediant_code,
                                   checkStock_id,
                                   checkStockDetail_description,
                                   checkStockDetail_quantity,
                                   checkStockDetail_unitExpire,
                                   checkStockDetail_valueLoss,
                                   checkStockDetail_date
                               )
                               VALUES (
                                   2,
                                   '2',
                                   '2',
                                   'เมล็ดกาแฟหมดไป 5 ถุง',
                                   5,
                                   0,
                                   5,
                                   '2021/02/03'
                               );


-- Table: CUSTOMER
DROP TABLE IF EXISTS CUSTOMER;

CREATE TABLE IF NOT EXISTS CUSTOMER (
    customer_id         INTEGER NOT NULL
                                UNIQUE,
    customer_status     TEXT    NOT NULL,
    customer_name       TEXT    NOT NULL,
    customer_lastname   TEXT    NOT NULL,
    customer_tel        TEXT    NOT NULL,
    customer_birth_date TEXT    NOT NULL,
    customer_point      INTEGER NOT NULL,
    customer_join_date  TEXT    NOT NULL,
    PRIMARY KEY (
        customer_id
    )
);

INSERT INTO CUSTOMER (
                         customer_id,
                         customer_status,
                         customer_name,
                         customer_lastname,
                         customer_tel,
                         customer_birth_date,
                         customer_point,
                         customer_join_date
                     )
                     VALUES (
                         1,
                         'active',
                         'Peter',
                         'Parker',
                         '0971543251',
                         '2002/02/02',
                         35,
                         '2021/01/01'
                     );

INSERT INTO CUSTOMER (
                         customer_id,
                         customer_status,
                         customer_name,
                         customer_lastname,
                         customer_tel,
                         customer_birth_date,
                         customer_point,
                         customer_join_date
                     )
                     VALUES (
                         2,
                         'active',
                         'John',
                         'Doe',
                         '0831517582',
                         '2000/01/01',
                         60,
                         '2021/03/15'
                     );


-- Table: EMPLOYEE
DROP TABLE IF EXISTS EMPLOYEE;

CREATE TABLE IF NOT EXISTS EMPLOYEE (
    employee_id            INTEGER NOT NULL
                                   UNIQUE,
    branch_id              TEXT    NOT NULL,
    employee_title         TEXT    NOT NULL,
    employee_name          TEXT    NOT NULL,
    employee_lastname      TEXT    NOT NULL,
    employee_tel           TEXT    NOT NULL,
    employee_birthDate     TEXT    NOT NULL,
    employee_minWork       INTEGER NOT NULL,
    employee_qualification TEXT    NOT NULL,
    employee_startDate     TEXT    NOT NULL,
    employee_moneyRate     REAL    NOT NULL,
    PRIMARY KEY (
        employee_id AUTOINCREMENT
    ),
    FOREIGN KEY (
        branch_id
    )
    REFERENCES BRANCH
);

INSERT INTO EMPLOYEE (
                         employee_id,
                         branch_id,
                         employee_title,
                         employee_name,
                         employee_lastname,
                         employee_tel,
                         employee_birthDate,
                         employee_minWork,
                         employee_qualification,
                         employee_startDate,
                         employee_moneyRate
                     )
                     VALUES (
                         1,
                         'BUU',
                         'owner',
                         'Jack',
                         'Maxman',
                         '0888888888',
                         '2023/10/05',
                         8,
                         'MIT',
                         '2020/10/05',
                         1000.0
                     );

INSERT INTO EMPLOYEE (
                         employee_id,
                         branch_id,
                         employee_title,
                         employee_name,
                         employee_lastname,
                         employee_tel,
                         employee_birthDate,
                         employee_minWork,
                         employee_qualification,
                         employee_startDate,
                         employee_moneyRate
                     )
                     VALUES (
                         2,
                         'BUU',
                         'employee',
                         'Tony',
                         'Stark',
                         '0812345680',
                         '2003/08/03',
                         6,
                         'BUU',
                         '2020/11/15',
                         400.0
                     );

INSERT INTO EMPLOYEE (
                         employee_id,
                         branch_id,
                         employee_title,
                         employee_name,
                         employee_lastname,
                         employee_tel,
                         employee_birthDate,
                         employee_minWork,
                         employee_qualification,
                         employee_startDate,
                         employee_moneyRate
                     )
                     VALUES (
                         3,
                         'BUU',
                         'employee',
                         'Walt',
                         'Disney',
                         '0812345679',
                         '2000/02/15',
                         6,
                         'BUU',
                         '2020/12/05',
                         400.0
                     );


-- Table: INGREDIENT
DROP TABLE IF EXISTS INGREDIENT;

CREATE TABLE IF NOT EXISTS INGREDIENT (
    ingredient_code     INTEGER NOT NULL
                                UNIQUE,
    ingredient_name     TEXT    NOT NULL,
    ingredient_minNeed  INTEGER NOT NULL,
    ingredient_quantity INTEGER NOT NULL,
    ingredient_value    REAL    NOT NULL,
    PRIMARY KEY (
        ingredient_code
    )
);

INSERT INTO INGREDIENT (
                           ingredient_code,
                           ingredient_name,
                           ingredient_minNeed,
                           ingredient_quantity,
                           ingredient_value
                       )
                       VALUES (
                           1,
                           'ผงชาเขียว',
                           10,
                           5,
                           10.0
                       );

INSERT INTO INGREDIENT (
                           ingredient_code,
                           ingredient_name,
                           ingredient_minNeed,
                           ingredient_quantity,
                           ingredient_value
                       )
                       VALUES (
                           2,
                           'เมล็ดกาแฟ',
                           10,
                           5,
                           10.0
                       );


-- Table: PRODUCT
DROP TABLE IF EXISTS PRODUCT;

CREATE TABLE IF NOT EXISTS PRODUCT (
    product_code        INTEGER NOT NULL
                                UNIQUE,
    product_name        TEXT    NOT NULL,
    product_price       REAL    NOT NULL,
    product_size        TEXT    NOT NULL,
    product_quantity    INT     NOT NULL,
    product_category    TEXT    NOT NULL,
    product_subCategory TEXT    NOT NULL,
    PRIMARY KEY (
        product_code
    )
);


-- Table: PROMOTION
DROP TABLE IF EXISTS PROMOTION;

CREATE TABLE IF NOT EXISTS PROMOTION (
    promotion_id          INTEGER NOT NULL
                                  UNIQUE,
    promotiont_name       TEXT    NOT NULL,
    promotion_description TEXT    NOT NULL,
    promotion_startDate   TEXT    NOT NULL,
    promotion_endDate     TEXT    NOT NULL,
    promotion_status      TEXT    NOT NULL,
    PRIMARY KEY (
        promotion_id
    )
);

INSERT INTO PROMOTION (
                          promotion_id,
                          promotiont_name,
                          promotion_description,
                          promotion_startDate,
                          promotion_endDate,
                          promotion_status
                      )
                      VALUES (
                          1,
                          'แต้มสะสม',
                          'ลดราคาสินค้าด้วยแต้มสะสม',
                          '2021/01/01',
                          '2021/01/01',
                          'active'
                      );

INSERT INTO PROMOTION (
                          promotion_id,
                          promotiont_name,
                          promotion_description,
                          promotion_startDate,
                          promotion_endDate,
                          promotion_status
                      )
                      VALUES (
                          2,
                          'วันเกิด',
                          'ลดราคาหากเป็นวันเกิดของสมาชิก',
                          '2021/01/01',
                          '2023/01/01',
                          'active'
                      );


-- Table: PROMOTION_DETAIL
DROP TABLE IF EXISTS PROMOTION_DETAIL;

CREATE TABLE IF NOT EXISTS PROMOTION_DETAIL (
    productDetail_id       INTEGER NOT NULL
                                   UNIQUE,
    product_code           TEXT    NOT NULL,
    promotion_id           TEXT    NOT NULL,
    productDetail_discount REAL    NOT NULL,
    productDetail_status   TEXT    NOT NULL,
    PRIMARY KEY (
        productDetail_id
    ),
    FOREIGN KEY (
        product_code
    )
    REFERENCES PRODUCT,
    FOREIGN KEY (
        promotion_id
    )
    REFERENCES PROMOTION_DETAIL
);


-- Table: RECEIPT
DROP TABLE IF EXISTS RECEIPT;

CREATE TABLE IF NOT EXISTS RECEIPT (
    receipt_id           INTEGER NOT NULL
                                 UNIQUE,
    customer_id          INTEGER NOT NULL,
    employee_id          INTEGER NOT NULL,
    branch_id            TEXT    NOT NULL,
    receipt_date         TEXT    NOT NULL,
    receipt_totalPrice   REAL    NOT NULL,
    receipt_discount     REAL    NOT NULL,
    receipt_netPrice     REAL    NOT NULL,
    receipt_moneyReceive REAL    NOT NULL,
    receipt_moneyChange  REAL    NOT NULL,
    PRIMARY KEY (
        receipt_id
    ),
    FOREIGN KEY (
        customer_id
    )
    REFERENCES CUSTOMER,
    FOREIGN KEY (
        employee_id
    )
    REFERENCES EMPLOYEE,
    FOREIGN KEY (
        branch_id
    )
    REFERENCES BRANCH
);

INSERT INTO RECEIPT (
                        receipt_id,
                        customer_id,
                        employee_id,
                        branch_id,
                        receipt_date,
                        receipt_totalPrice,
                        receipt_discount,
                        receipt_netPrice,
                        receipt_moneyReceive,
                        receipt_moneyChange
                    )
                    VALUES (
                        1,
                        1,
                        1,
                        'BUU',
                        '2021/02/03',
                        95.0,
                        35.0,
                        60.0,
                        60.0,
                        0.0
                    );

INSERT INTO RECEIPT (
                        receipt_id,
                        customer_id,
                        employee_id,
                        branch_id,
                        receipt_date,
                        receipt_totalPrice,
                        receipt_discount,
                        receipt_netPrice,
                        receipt_moneyReceive,
                        receipt_moneyChange
                    )
                    VALUES (
                        2,
                        2,
                        2,
                        'BUU',
                        '2021/02/03',
                        55.0,
                        0.0,
                        55.0,
                        100.0,
                        45.0
                    );


-- Table: RECEIPT_DETAIL
DROP TABLE IF EXISTS RECEIPT_DETAIL;

CREATE TABLE IF NOT EXISTS RECEIPT_DETAIL (
    receiptDetail_id        INTEGER NOT NULL
                                    UNIQUE,
    product_code            TEXT,
    promotion_id            TEXT,
    promotionDetail_id      TEXT,
    receipt_id              TEXT,
    receiptDetail_unit      INTEGER NOT NULL,
    receiptDetail_unitPrice REAL    NOT NULL,
    receiptDetail_discount  REAL    NOT NULL,
    receiptDetail_netPrice  REAL    NOT NULL,
    PRIMARY KEY (
        receiptDetail_id
    ),
    FOREIGN KEY (
        product_code
    )
    REFERENCES PRODUCT,
    FOREIGN KEY (
        promotion_id
    )
    REFERENCES PROMOTION,
    FOREIGN KEY (
        promotion_id
    )
    REFERENCES PROMOTION_DETAIL
);


-- Table: RECEIPT_STOCK
DROP TABLE IF EXISTS RECEIPT_STOCK;

CREATE TABLE IF NOT EXISTS RECEIPT_STOCK (
    receiptStock_id          INTEGER NOT NULL
                                     UNIQUE,
    employee_id              INTEGER,
    vendor_code              TEXT,
    receiptStock_quantity    INT     NOT NULL,
    receiptStock_total       REAL    NOT NULL,
    receiptStock_discount    REAL    NOT NULL,
    receiptStock_netPrice    REAL    NOT NULL,
    receiptStock_dateOrder   TEXT    NOT NULL,
    receiptStock_dateReceive TEXT    NOT NULL,
    receiptStock_datePaid    TEXT    NOT NULL,
    receiptStock_change      REAL    NOT NULL,
    receiptStock_status      TEXT    NOT NULL,
    PRIMARY KEY (
        receiptStock_id
    ),
    FOREIGN KEY (
        employee_id
    )
    REFERENCES EMPLOYEE,
    FOREIGN KEY (
        vendor_code
    )
    REFERENCES VENDOR
);

INSERT INTO RECEIPT_STOCK (
                              receiptStock_id,
                              employee_id,
                              vendor_code,
                              receiptStock_quantity,
                              receiptStock_total,
                              receiptStock_discount,
                              receiptStock_netPrice,
                              receiptStock_dateOrder,
                              receiptStock_dateReceive,
                              receiptStock_datePaid,
                              receiptStock_change,
                              receiptStock_status
                          )
                          VALUES (
                              1,
                              1,
                              '1',
                              1,
                              5.0,
                              0.0,
                              325.0,
                              '2021/02/03',
                              '2021/02/05',
                              '2021/02/03',
                              0.0,
                              'recieved'
                          );

INSERT INTO RECEIPT_STOCK (
                              receiptStock_id,
                              employee_id,
                              vendor_code,
                              receiptStock_quantity,
                              receiptStock_total,
                              receiptStock_discount,
                              receiptStock_netPrice,
                              receiptStock_dateOrder,
                              receiptStock_dateReceive,
                              receiptStock_datePaid,
                              receiptStock_change,
                              receiptStock_status
                          )
                          VALUES (
                              2,
                              2,
                              '2',
                              1,
                              5.0,
                              0.0,
                              500.0,
                              '2021/03/03',
                              '2021/03/05',
                              '2021/03/03',
                              0.0,
                              'recieved'
                          );


-- Table: RECEIPT_STOCK_DETAIL
DROP TABLE IF EXISTS RECEIPT_STOCK_DETAIL;

CREATE TABLE IF NOT EXISTS RECEIPT_STOCK_DETAIL (
    receiptStockDetail_id           TEXT    NOT NULL
                                            UNIQUE,
    receiptStock_id                 TEXT,
    ingrediant_code                 TEXT,
    receiptStockDetail_quantity     INTEGER NOT NULL,
    receiptStockDetail_description  TEXT    NOT NULL,
    receiptStockDetail_pricePerUnit REAL    NOT NULL,
    receiptStockDetail_discount     REAL    NOT NULL,
    receiptStockDetail_netPrice     REAL    NOT NULL,
    PRIMARY KEY (
        receiptStockDetail_id
    ),
    FOREIGN KEY (
        receiptStock_id
    )
    REFERENCES RECEIPT_STOCK,
    FOREIGN KEY (
        ingrediant_code
    )
    REFERENCES INGREDIENT
);

INSERT INTO RECEIPT_STOCK_DETAIL (
                                     receiptStockDetail_id,
                                     receiptStock_id,
                                     ingrediant_code,
                                     receiptStockDetail_quantity,
                                     receiptStockDetail_description,
                                     receiptStockDetail_pricePerUnit,
                                     receiptStockDetail_discount,
                                     receiptStockDetail_netPrice
                                 )
                                 VALUES (
                                     '1',
                                     '1',
                                     '1',
                                     1,
                                     'ผงชาเขียว 5 ถุง',
                                     65.0,
                                     0.0,
                                     325.0
                                 );

INSERT INTO RECEIPT_STOCK_DETAIL (
                                     receiptStockDetail_id,
                                     receiptStock_id,
                                     ingrediant_code,
                                     receiptStockDetail_quantity,
                                     receiptStockDetail_description,
                                     receiptStockDetail_pricePerUnit,
                                     receiptStockDetail_discount,
                                     receiptStockDetail_netPrice
                                 )
                                 VALUES (
                                     '2',
                                     '2',
                                     '2',
                                     2,
                                     'เมล็ดกาแฟ 5 ถุง',
                                     100.0,
                                     0.0,
                                     500.0
                                 );


-- Table: SALRAY_PAYMENT
DROP TABLE IF EXISTS SALRAY_PAYMENT;

CREATE TABLE IF NOT EXISTS SALRAY_PAYMENT (
    salaryPayment_id          INTEGER NOT NULL
                                      UNIQUE,
    employee_id               INTEGER,
    salaryPayment_date        TEXT    NOT NULL,
    salaryPayment_type        TEXT    NOT NULL,
    salaryPayment_totalAmount REAL    NOT NULL,
    PRIMARY KEY (
        salaryPayment_id
    ),
    FOREIGN KEY (
        employee_id
    )
    REFERENCES EMPLOYEE
);

INSERT INTO SALRAY_PAYMENT (
                               salaryPayment_id,
                               employee_id,
                               salaryPayment_date,
                               salaryPayment_type,
                               salaryPayment_totalAmount
                           )
                           VALUES (
                               1,
                               1,
                               '2021/01/25',
                               'รายเดือน',
                               50000.0
                           );

INSERT INTO SALRAY_PAYMENT (
                               salaryPayment_id,
                               employee_id,
                               salaryPayment_date,
                               salaryPayment_type,
                               salaryPayment_totalAmount
                           )
                           VALUES (
                               2,
                               2,
                               '2021/01/25',
                               'รายเดือน',
                               12000.0
                           );

INSERT INTO SALRAY_PAYMENT (
                               salaryPayment_id,
                               employee_id,
                               salaryPayment_date,
                               salaryPayment_type,
                               salaryPayment_totalAmount
                           )
                           VALUES (
                               3,
                               3,
                               '2021/01/25',
                               'รายเดือน',
                               12000.0
                           );


-- Table: USER
DROP TABLE IF EXISTS USER;

CREATE TABLE IF NOT EXISTS USER (
    user_id       INTEGER NOT NULL
                          UNIQUE,
    employee_id   INTEGER,
    user_login    TEXT    NOT NULL,
    user_password TEXT    NOT NULL,
    user_title    TEXT    NOT NULL,
    user_gender   TEXT    NOT NULL,
    PRIMARY KEY (
        user_id AUTOINCREMENT
    ),
    FOREIGN KEY (
        employee_id
    )
    REFERENCES EMPLOYEE
);

INSERT INTO USER (
                     user_id,
                     employee_id,
                     user_login,
                     user_password,
                     user_title,
                     user_gender
                 )
                 VALUES (
                     1,
                     1,
                     'admin',
                     'myadmin',
                     'owner',
                     'Male'
                 );

INSERT INTO USER (
                     user_id,
                     employee_id,
                     user_login,
                     user_password,
                     user_title,
                     user_gender
                 )
                 VALUES (
                     2,
                     2,
                     'tony',
                     'tony12345',
                     'employee',
                     'Male'
                 );

INSERT INTO USER (
                     user_id,
                     employee_id,
                     user_login,
                     user_password,
                     user_title,
                     user_gender
                 )
                 VALUES (
                     3,
                     3,
                     'walt',
                     'walt45678',
                     'employee',
                     'Male'
                 );


-- Table: VENDOR
DROP TABLE IF EXISTS VENDOR;

CREATE TABLE IF NOT EXISTS VENDOR (
    vendor_code       TEXT    NOT NULL
                              UNIQUE,
    vendor_name       TEXT    NOT NULL,
    vendor_contact    TEXT    NOT NULL,
    vendor_locate     TEXT    NOT NULL,
    vendor_tel        TEXT    NOT NULL,
    vendor_state      TEXT    NOT NULL,
    vendor_orderCount INTEGER NOT NULL,
    PRIMARY KEY (
        vendor_code
    )
);

INSERT INTO VENDOR (
                       vendor_code,
                       vendor_name,
                       vendor_contact,
                       vendor_locate,
                       vendor_tel,
                       vendor_state,
                       vendor_orderCount
                   )
                   VALUES (
                       '1',
                       'MAKRO',
                       'store5@siammakro.co.th',
                       '55/3 หมู่ 2 ถนนสุขุมวิท ตำบลเสม็ด อำเภอเมือง จังหวัดชลบุรี 20000',
                       '0871234567',
                       'Chonburi',
                       1
                   );

INSERT INTO VENDOR (
                       vendor_code,
                       vendor_name,
                       vendor_contact,
                       vendor_locate,
                       vendor_tel,
                       vendor_state,
                       vendor_orderCount
                   )
                   VALUES (
                       '2',
                       'LOTUS',
                       'tescolotus.com',
                       '62 หมู่ที่ 1 ถ. สุขุมวิท ตำบล เสม็ด อำเภอเมืองชลบุรี ชลบุรี 20000',
                       '038282572',
                       'Chonburi',
                       1
                   );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
